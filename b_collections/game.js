let countries = {
  fr: "France",
  be: "Belgique",
  lu: "Luxembourg",
  "us-ca": "Californie",
  "us-ks": "Kansas",
};

let countryCodes = Object.keys(countries).filter((c) => !c.startsWith("us-"));

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

console.log(randomInt(20));
console.log(countryCodes);
