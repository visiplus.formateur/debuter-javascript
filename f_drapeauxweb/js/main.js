import * as game from "./game.js";
import * as display from "./display.js";

function verifyAnswer() {
  const userAnswerIndex = this.id.slice(-1);
  const userAnswer = game.state.answerCodes[userAnswerIndex];
  game.checkAnswerAndUpdateScore(userAnswer);
  if (game.state.currentQuestion <= game.state.totalQuestions) {
    game.nextQuestion();
    display.updateState(game.state);
  } else {
    display.mainMessage(`GAME OVER - SCORE : ${game.state.score}`);
    gameIntro();
  }
}

function startGame() {
  display.gameMode();
  let actionButton = document.querySelector("#action");
  actionButton.onclick = gameIntro;
  game.newGame(10);
  document
    .querySelectorAll("#answers button")
    .forEach((button) => (button.onclick = verifyAnswer));
  display.updateState(game.state);
}

function gameIntro() {
  display.welcomeMode();
  let actionButton = document.querySelector("#action");
  actionButton.onclick = startGame;
}

window.onload = gameIntro;
