import { countries, countryCodes } from "./data.js";

export function mainMessage(msg) {
  document.querySelector("#score").textContent = msg;
}

export function welcomeMode() {
  document.querySelector("#action").textContent = "▶️ NOUVEAU JEU";
  document.querySelector("#content").classList.add("hidden");
}

export function gameMode() {
  updateScore(0);
  document.querySelector("#action").textContent = "⏹️ QUITTER";
  document.querySelector("#content").classList.remove("hidden");
}

export function updateScore(score) {
  document.querySelector("#score").textContent = `SCORE ${score}`;
}

export function updateState(state) {
  updateScore(state.score);
  document.querySelector("#quiz_content").textContent =
    countries[state.questionCode].toUpperCase();
  for (let index in state.answerCodes) {
    document.querySelector(`#answer${index}`).innerHTML = `<img
    src="https://flagcdn.com/224x168/${state.answerCodes[index]}.png"
    alt="Réponse ${index}" />`;
  }
}
