import * as game from "./game.js";

let gameState = game.newGame(10);
console.log(gameState);
let goodAnswer = game.checkAnswerAndUpdateScore(gameState.questionCode);
console.log(goodAnswer);
console.log(gameState);
gameState = game.nextQuestion();
console.log(gameState);
