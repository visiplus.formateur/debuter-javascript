import * as game from "./game.js";
import * as display from "./display.js";
import * as data from "./data.js";

let timeout = false;
let timerInterval;

function verifyAnswer() {
  if (timeout) {
    return;
  }
  clearInterval(timerInterval);
  const userAnswerIndex = this.id.slice(-1);
  const userAnswer = game.state.answerCodes[userAnswerIndex];
  const isGood = game.checkAnswerAndUpdateScore(userAnswer);
  display.hightlightAnswer(userAnswerIndex, isGood);
  setTimeout(nextGameStep, 2000);
}

function startTimer() {
  let time = 5000;
  timerInterval = setInterval(() => {
    time -= 30;
    if (time <= 0) {
      clearInterval(timerInterval);
      questionTimeout();
    } else {
      display.drawRemainingTime(time / 5000);
    }
  }, 30);
}

function nextGameStep() {
  if (game.state.currentQuestion <= game.state.totalQuestions) {
    game.nextQuestion();
    display.updateState(game.state);
    startTimer();
  } else {
    display.mainMessage(`GAME OVER - SCORE : ${game.state.score}`);
    gameIntro();
  }
  timeout = false;
}

function questionTimeout() {
  timeout = true;
  nextGameStep();
}

function startGame() {
  display.gameMode();
  let actionButton = document.querySelector("#action");
  actionButton.onclick = gameIntro;
  game.newGame(10);
  document
    .querySelectorAll("#answers button")
    .forEach((button) => (button.onclick = verifyAnswer));
  display.updateState(game.state);
  startTimer();
}

function gameIntro() {
  display.welcomeMode();
  let actionButton = document.querySelector("#action");
  actionButton.onclick = startGame;
}

data.loadCountries();
window.onload = gameIntro;
