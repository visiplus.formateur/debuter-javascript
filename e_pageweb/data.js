export let countries = {
  fr: "France",
  be: "Belgique",
  lu: "Luxembourg",
  es: "Espagne",
  it: "Italie",
  "us-ca": "Californie",
  "us-ks": "Kansas",
};

export let countryCodes = Object.keys(countries).filter(
  (c) => !c.startsWith("us-")
);
