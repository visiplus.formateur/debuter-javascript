import * as game from "./game.js";
import { countries, countryCodes } from "./data.js";

function updateState() {
  game.nextQuestion();
  document.querySelector("#question").textContent =
    countries[game.state.questionCode].toUpperCase();
  for (let index in game.state.answerCodes) {
    document.querySelector(`#answer${index}`).innerHTML = `<img
    src="https://flagcdn.com/224x168/${game.state.answerCodes[index]}.png"
    alt="Réponse ${index}" />`;
  }
}

function startGame() {
  game.newGame(10);
  document
    .querySelectorAll("#answers button")
    .forEach((button) => (button.onclick = updateState));
  updateState();
}

function gameIntro() {
  let actionButton = document.querySelector("#action");
  actionButton.onclick = startGame;
}

window.onload = gameIntro;
